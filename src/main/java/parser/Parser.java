package parser;

import extensionchecker.Checker;
import elements.Paper;
import parser.dom.DOMParser;
import java.nio.file.Paths;
import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Comparator;
import java.util.Scanner;
import javax.xml.transform.*;
import java.io.*;

public class Parser {

  public static void main  (String[] args) {
      File xml = new File("src\\main\\resources\\papers.xml");
      //File xml = new File(Paths.get(ClassLoader.getSystemResource("papers.xml").toURI()).toString());
      //File xsd = new File(ClassLoader.getSystemResource("papers.xsd").toString());
      File xsd = new File("src\\main\\resources\\papers.xsd");

      if (checkIfXML(xml) && checkIfXSD(xsd)) {
          printList(DOMParser.getPaperList(xml, xsd), "DOM:");
      }
   System.out.println("If you want to convert xml file to html - enter 'h' - and return");
   Scanner sc = new Scanner(System.in);
   if (sc.nextLine().equals("h")) {
   convertToHtml();
   System.out.print("Html file created");
   }
   
   System.out.println("If you want to change the root element of xml - enter 'r' - and return");
   if (sc.nextLine().equals("r")) {
	   changetheRoot();
	   System.out.print("Root changed in xml file - papers to publishings");
	   }

  sc.close();
  }

  private static boolean checkIfXSD(File xsd) {
    return Checker.isXSD(xsd);
  }
  private static boolean checkIfXML(File xml) {
    return Checker.isXML(xml);
  }

  private static void printList(List<Paper> papers, String parserName) {
	  Comparator<Paper> comparatorByName
      = Comparator.comparing(Paper::getName);
	Collections.sort(papers,comparatorByName);
    System.out.println("Here is the content of xml file: ");
    System.out.println("Parsed by "+parserName);
    for (Paper paper : papers) {
      System.out.println(paper);
    }
  }
  
  private static void convertToHtml() {
      try {
          TransformerFactory tFactory = TransformerFactory.newInstance();
          Transformer transformer = tFactory.newTransformer
                  (new javax.xml.transform.stream.StreamSource("src\\main\\resources\\xmltohtml.xsl"));
          // (ClassLoader.getSystemResource("xmltohtml.xsl").toString()));
          transformer.transform(new javax.xml.transform.stream.StreamSource("src\\main\\resources\\papers.xml"),
                  new javax.xml.transform.stream.StreamResult(new FileOutputStream("src\\main\\resources\\papers.html")));
      } catch (Exception e) {
          e.printStackTrace();
      }
  }
  
  
  private static void changetheRoot() {
	  //change the root "papers" element to - "publishings" root element
      try {
          TransformerFactory tFactory = TransformerFactory.newInstance();
          Transformer transformer = tFactory.newTransformer
                  (new javax.xml.transform.stream.StreamSource("src\\main\\resources\\changeroot.xsl"));
          transformer.transform(new javax.xml.transform.stream.StreamSource("src\\main\\resources\\papers.xml"),
                  new javax.xml.transform.stream.StreamResult(new FileOutputStream("src\\main\\resources\\papers2.xml")));
      } catch (Exception e) {
          e.printStackTrace();
      }
  }
}