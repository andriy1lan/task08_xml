package parser.dom;


import elements.Paper;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class DOMParser {
    public static List<Paper> getPaperList(File xml, File xsd){
        DOMDocCreator creator = new DOMDocCreator(xml);
        Document doc = creator.getDocument();

   try {
         DOMValidator.validate(DOMValidator.createSchema(xsd),doc);
         System.out.println("papers.xml is validated againsts its papers.xsd (xml schema definition)");
    }
     catch (IOException | SAXException ex){
    	 System.out.println("xml file is not validated acorrfingly to its xsd");
         ex.printStackTrace();
     }

        DOMDocReader reader = new DOMDocReader();
        return reader.readDoc(doc);
    }
}