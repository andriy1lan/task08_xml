package parser.dom;

import elements.Paper;
import elements.Chars;
import elements.SubscriptIndex;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMDocReader {
    public List<Paper> readDoc(Document doc){
        doc.getDocumentElement().normalize();
        List<Paper> papers = new ArrayList<Paper>();
        NodeList nodeList = doc.getElementsByTagName("paper");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Paper paper = new Paper();
            Chars chars;
            SubscriptIndex index;

            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;

                paper.setPaperCode(Integer.parseInt(element.getAttribute("paperCode")));
                paper.setName(element.getElementsByTagName("name").item(0).getTextContent());
                paper.setType(element.getElementsByTagName("type").item(0).getTextContent());
                paper.setMonthly(Boolean.parseBoolean(element.getElementsByTagName("monthly").item(0).getTextContent()));
                
                chars = getChars(element.getElementsByTagName("chars"));
                index = getSubscriptIndex(element.getElementsByTagName("subscriptIndex"));

                chars.setSubscriptIndex(index);
                paper.setChars(chars);
                papers.add(paper);
            }
        }
        return papers;
    }

    private Chars getChars(NodeList nodes){
        Chars chars = new Chars();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            chars.setColor(Boolean.parseBoolean(element.getElementsByTagName("color").item(0).getTextContent()));
            chars.setGlossy(Boolean.parseBoolean(element.getElementsByTagName("glossy").item(0).getTextContent()));
            chars.setSize(Integer.parseInt(element.getElementsByTagName("size").item(0).getTextContent()));
        }

        return chars;
    }

    private SubscriptIndex getSubscriptIndex(NodeList nodes){
    	SubscriptIndex sindex = new SubscriptIndex();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            sindex.setHasIndex(Boolean.parseBoolean(element.getElementsByTagName("hasIndex").item(0).getTextContent()));
            if (element.getElementsByTagName("index").item(0).getTextContent()!="")
            sindex.setIndex(Integer.parseInt(element.getElementsByTagName("index").item(0).getTextContent()));
            else sindex.setIndex(0);
        }
        return sindex;
    }
}
