<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
<html>
<head><title>ConvertXMLtoHTML</title></head>
<body>
<table border="1" caption="Ukrainian Papers">
<tr align="center">
The List of Ukrainian Papers
</tr>
<tr>
<th>PaperCode</th>
<th>Name</th>
<th>Type</th>
<th>Monthy</th>
<th>Color</th>
<th>Glossy</th>
<th>Size</th>
<th>HasIndex</th>
<th>Index</th>
</tr>
<xsl:for-each select="papers/paper">
<xsl:sort select="name"/>
<tr>
<td><xsl:value-of select="@paperCode"/></td>
<td><xsl:value-of select="name"/></td>
<td><xsl:value-of select="type"/></td>
<td><xsl:value-of select="monthly"/></td>
<td><xsl:value-of select="chars/color"/></td>
<td><xsl:value-of select="chars/glossy"/></td>
<td><xsl:value-of select="chars/size"/></td>
<td><xsl:value-of select="chars/subscriptIndex/hasIndex"/></td>
<td><xsl:value-of select="chars/subscriptIndex/index"/></td>
</tr>
</xsl:for-each>
</table>
</body></html>
</xsl:template>
</xsl:stylesheet>